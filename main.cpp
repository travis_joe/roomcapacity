/*
 * Travis Joe
 * CS 2
 * Assignment 2.6 - Room Capacity
 * Given the maximum capacity, and current number of people in a room, output whether the room meets regulation (number of people < max capacity)
 */
#include <iostream>

using namespace std;

int main() {
    int maximumCapacity = 0; // Maximum capacity of the room
    int currentPeople = 0; // Number of people currently in the room

    cout << "Enter the maximum room capacity: ";
    cin >> maximumCapacity;

    cout << "Enter the number of people currently in the room: ";
    cin >> currentPeople;

    if(currentPeople > maximumCapacity) {
        int mustLeave = currentPeople - maximumCapacity; // number of people who must leave the room
        cout << "There are too many people in the room! " << mustLeave << " people must leave the room.\n";
    } else {
        int canEnter = maximumCapacity - currentPeople; // number of people who can enter the room
        cout << "The maximum capacity has not been reached!\n";
        cout << canEnter << " more people can enter the room.\n";
    }
}